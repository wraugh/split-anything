#!/usr/bin/env node
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const fs = require('fs')
const { code2doc } = require('marcco')
const { rewriteRequires } = require('public-requires')
const snippin = require('snippin')
const ST = require('stream-template')

// presentation helpers
const s = snippin()
const rr = src => rewriteRequires('split-anything', __dirname, __dirname, src)
const js2doc = src => code2doc(rr(src), { codePrefix: '~~~javascript' })
const pin = (file, snippet) => s.getSnippets(file).then(snippets => snippets[snippet])

process.chdir(__dirname)
const out = fs.createWriteStream('./README.md')

ST`**split-anything** lets you read files line by line synchronously. As a bonus,
you can use it

 - on strings like [String.split](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split),
 - as a stream transform like [split2](https://github.com/mcollina/split2),
 - to read files line by line _asynchronously_, or
 - with your own interface around the underlying SplitAnything class.

What sets it apart from other text-splitting utilities is that it preserves line
endings by default, unlike [String.split](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split)-based
solutions.

String interface - splitStr
---------------------------

${pin('test.js', 'splitStr').then(s => js2doc(s))}

File interface - SplitReader
----------------------------

${pin('test.js', 'SplitReader').then(s => js2doc(s))}

Stream interface - SplitTransform
---------------------------------

${pin('test.js', 'SplitTransform').then(s => js2doc(s))}

Generic interface - SplitAnything
---------------------------------

${pin('test.js', 'SplitAnything').then(s => js2doc(s))}

Contributing
------------

This project is left deliberately imperfect to encourage you to participate in
its development. If you make a Pull Request that

 - explains and solves a problem,
 - follows [standard style](https://standardjs.com/), and
 - maintains 100% test coverage

it _will_ be merged: this project follows the
[C4 process](https://rfc.zeromq.org/spec:42/C4/).

To make sure your commits follow the style guide and pass all tests, you can add

    ./.pre-commit

to your git pre-commit hook.
`.pipe(out)
