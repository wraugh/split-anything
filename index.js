/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const fs = require('fs')
const { Transform } = require('readable-stream')
const { StringDecoder } = require('string_decoder')

class SplitAnything {
  constructor (sep = /\n/, chomp = false) {
    this._sep = sep
    this._chomp = chomp
    this._lines = []
    this._buf = ''
  }

  getLine (last = false) {
    if (last && this._lines.length === 0 && this._buf !== '') {
      this._lines.push(this._buf)
      this._buf = ''
    }

    return this._lines.shift()
  }

  cat (str) {
    this._lines = this._lines.concat(this._parse(this._buf + str))
    this._buf = this._lines.pop()

    return this
  }

  _parse (str) {
    const lines = []
    let m, line, s

    while ((m = this._sep.exec(str), m !== null)) {
      s = m.index + m[0].length
      line = str.substring(0, this._chomp ? m.index : s)
      str = str.substring(s)
      lines.push(line)
    }

    lines.push(str)

    return lines
  }
}

class SplitReader {
  constructor (file, encoding = 'utf8', sep = /\n/, chomp = false) {
    if (typeof file === 'string') {
      file = fs.openSync(file)
    }
    if (!Number.isInteger(file)) {
      throw new Error(`Failed to prepare ${JSON.stringify(file)} for reading`)
    }
    this._fd = file
    this._decoder = new StringDecoder(encoding)
    this._sa = new SplitAnything(sep, chomp)
  }

  read (bufLength = 250) {
    return new Promise((resolve, reject) => {
      const buf = this._getBuffer(bufLength)
      if (!(buf instanceof Buffer)) {
        return resolve(buf)
      }
      fs.read(this._fd, buf, 0, bufLength, null, (err, bytesRead, buf) => {
        if (err) return reject(err)
        this._catBuffer(buf, bytesRead)

        resolve(this.read(bufLength))
      })
    })
  }

  readSync (bufLength = 250) {
    const buf = this._getBuffer(bufLength)
    if (!(buf instanceof Buffer)) {
      return buf
    }
    const bytesRead = fs.readSync(this._fd, buf, 0, bufLength, null)
    this._catBuffer(buf, bytesRead)

    return this.readSync(bufLength)
  }

  // Returns a buffered line, or a Buffer in which to read if there are none.
  // I'm abusing the double meaning of the word "buffer" here, referring either
  // to a buffered line, or to a Buffer in which to read bytes.
  _getBuffer (bufLength) {
    const line = this._sa.getLine(this._eofReached)
    if (line !== undefined) {
      return line
    }
    if (this._eofReached) {
      return null
    }

    return Buffer.alloc(bufLength)
  }

  _catBuffer (buf, bytesRead) {
    const bytes = buf.slice(0, bytesRead)
    this._eofReached = bytesRead === 0
    this._sa.cat(this._eofReached
      ? this._decoder.end(bytes)
      : this._decoder.write(bytes))
    if (this._eofReached) {
      fs.close(this._fd, () => 'NOP')
    }
  }
}

class SplitTransform extends Transform {
  constructor (sep = /\n/, chomp = false, streamOptions = {}) {
    super(Object.assign({}, streamOptions, {
      transform (chunk, encoding, callback) {
        let line
        this._sa.cat(this._decoder.write(chunk))
        while ((line = this._sa.getLine(), line !== undefined)) {
          this.push(line)
        }
        callback()
      },
      flush (callback) {
        let line
        while ((line = this._sa.getLine(true), line !== undefined)) {
          this.push(line)
        }
        callback()
      }
    }))
    this._sa = new SplitAnything(sep, chomp)
    this._decoder = new StringDecoder(this._writableState.defaultEncoding)
    this.setEncoding('utf8')
  }
}

function splitStr (str, sep = /\n/, chomp = false) {
  let line
  const lines = []
  const sa = new SplitAnything(sep, chomp)
  sa.cat(str)
  while ((line = sa.getLine(true), line !== undefined)) {
    lines.push(line)
  }

  return lines
}

module.exports.SplitAnything = SplitAnything
module.exports.SplitReader = SplitReader
module.exports.SplitTransform = SplitTransform
module.exports.splitStr = splitStr
